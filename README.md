# Geo coding app

The app allows user to search addresses using Google Geocoding API and check information about any point on the map by long pressing the point of interest

## High Level Architecture

The app consists of SPA built on [React](https://reactjs.org/) and REST API built on [fastify](https://fastify.io). In order to run this app, you need to run both services separately.

### API

The API is split into modules (i.e., features). Each module is a fastify plugin which can be easily included or excluded from the API.

### UI

The SPA is quite small, that's why I used a standard directory layout for small-medium apps (i.e., there is no point to split it into modules)

## Challenges

### API

It's more a constraint than a challenge but for such kind of API the main concern is an availability of 3rd party API, in our case Google Geocoding API. If this service doesn't work, our app becomes unusable.

So, to decrease the influence of Google Geocoding API:

1. I abstracted it as a service with static interface, so we can easily switch to another geocoding provider when this one becomes unavailable, for example to [MapBox geocoding](https://docs.mapbox.com/api/search/)
2. Added LRU cache, so the most common geocoding requests can be returned without even touching 3rd party API (obviously, for better cache hitting it requires additional work)
3. Used circuit breaker pattern, so if Google API doesn't respond, the API returns failed result to users without putting additional pressure on Google API.

Another issue which may appear in the future is the synchronous nature of `GET /api/v1/distance` endpoint. So, depending on the future traffic, it needs to become async.

### UI

The main challenge here was absence of requirements. I could mimic implementation of Google Maps (including searching, directions API and geolocation) but this would require more time which I'm not sure worth to be spent on.

## Requirements

* Nodejs 12.x+
* yarn 1.x+ (these apps uses yar.lock to ensure that we have the same dependencies across different envs)

## Installation

```sh
(cd packages/api && yarn);
(cd packages/ui && yarn);
```

## Configuration

Both parts of the app requires `GOOGLE_API_KEY` variable to contain valid [Google API key](https://support.google.com/googleapi/answer/6158862?hl=en)

## Run

```sh
export GOOGLE_API_KEY=... # valid Google API KEY
cd packages/api && yarn run build && yarn start
cd packages/ui && yarn start
```

## Tests

To run e2e UI tests:

```sh
(cd packages/ui && yarn run test.e2e);
```

To run server side integration tests:

```sh
(cd packages/api && yarn run test.e2e);
```

## License

MIT

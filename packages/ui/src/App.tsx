import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import SearchDrawer from './components/SearchDrawer';
import Map from './components/Map';
import { SearchResult } from './services/geo';

const useStyles = makeStyles(() => createStyles({
  root: {
    background: '#fff',
    boxShadow: 'rgba(0, 0, 0, 0.3) 0px 1px 4px -1px',
    margin: '8px'
  },
}));

export default () => {
  const [isOpenDrawer, setIsOpenDrawer] = React.useState(false);
  const [results, setResults] = React.useState<SearchResult[]>([]);
  const close = () => setIsOpenDrawer(false);
  const styles = useStyles();

  return (
    <div className="App">
      <Map results={results}>
        <IconButton className={styles.root} onClick={() => setIsOpenDrawer(true)}>
          <SearchIcon />
        </IconButton>
      </Map>
      <SearchDrawer
        open={isOpenDrawer}
        onClose={close}
        onFound={setResults}
      />
    </div>
  );
};

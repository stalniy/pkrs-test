import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ClearIcon from '@material-ui/icons/Clear';
import RingLoader from 'react-spinners/RingLoader';

const useStyles = makeStyles(theme => createStyles({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

export interface SearchInputProps {
  value: string
  isLoading?: boolean
  onChange(value: string): void
  onSearch?(): void
  onBack?(event: {}): void
}

export default function SearchInput(props: SearchInputProps) {
  const classes = useStyles();
  const submit = (event?: React.FormEvent<HTMLDivElement>) => {
    event && event.preventDefault();
    props.onSearch && props.onSearch();
  };
  const hasValue = !!props.value.trim();
  const resetOrBack = (event: {}) => {
    if (hasValue) {
      props.onChange('');
    } else if (props.onBack) {
      props.onBack(event);
    }
  };

  let icon;
  if (props.isLoading) {
    icon = <RingLoader size="18px" />;
  } else if (props.value.trim()) {
    icon = <ClearIcon />
  } else {
    icon = <ChevronLeftIcon />
  }

  return (
    <Paper component="form" className={classes.root} onSubmit={submit}>
      <InputBase
        className={classes.input}
        placeholder="The street address or plus code that you want to find"
        value={props.value}
        onChange={event => props.onChange(event.target.value)}
      />
      <IconButton type="submit" className={classes.iconButton} aria-label="search">
        <SearchIcon />
      </IconButton>
      <Divider className={classes.divider} orientation="vertical" />
      <IconButton className={classes.iconButton} onClick={resetOrBack}>
        {icon}
      </IconButton>
    </Paper>
  );
}

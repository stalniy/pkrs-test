import React from 'react';
import Drawer, { DrawerProps } from '@material-ui/core/Drawer';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import SearchInput from './SearchInput';
import geo, { SearchResult } from '../services/geo';

const useStyles = makeStyles(theme => createStyles({
  root: {
    width: 420,
    bottom: 'auto !important'
  },
  paper: {
    background: 'transparent',
    boxShadow: 'none',
    height: 'auto'
  },
  details: {
    padding: theme.spacing(1),
  },
  state: {
    maxWidth: 400,
  },
  heading: {
    marginBottom: theme.spacing(1)
  }
}));

type SearchState = null | 'loading' | 'found' | 'notfound' | 'error';
interface SearchDrawerProps extends DrawerProps {
  onFound?(results: SearchResult[]): void
}

function SearchState(props: { value: SearchState, query: string }) {
  const styles = useStyles();

  switch (props.value) {
  case 'notfound':
    return (
      <div className={styles.state}>
        <h4 className={styles.heading}>Can't find "{props.query}"</h4>
        <small>
          Make sure your search contains valid street name.
          Try adding a city, state, country, zip code or use
          <a href="https://plus.codes/" target="_blank" rel="noopener noreferrer">plus code.</a>
        </small>
      </div>
    );
  case 'error':
    return (
      <div className={styles.state}>
        <h4 className={styles.heading}>Service is temporary unavailable</h4>
        <small>
          Make sure you have a stable internet connection.
        </small>
      </div>
    );
  default:
    return null;
  }
}

export default React.forwardRef(({ onFound, ...props }: SearchDrawerProps, ref) => {
  const [query, setQuery] = React.useState('');
  const [searchState, setSearchState] = React.useState<SearchState>(null);
  const styles = useStyles();
  const search = () => {
    setSearchState('loading');
    geo.search(query)
      .then((results) => {
        setSearchState(results.length > 0 ? 'found' : 'notfound');
        onFound && onFound(results);
      })
      .catch(() => setSearchState('error'));
  };
  React.useEffect(() => {
    setSearchState(null);

    if (!query) {
      onFound && onFound([]);
    }
  }, [query]);

  return (
    <Drawer {...props} hideBackdrop classes={{ root: styles.root, paper: styles.paper }} ref={ref}>
      <div className={styles.details}>
        <SearchInput
          value={query}
          isLoading={searchState === 'loading'}
          onChange={setQuery}
          onSearch={search}
          onBack={event => props.onClose!(event, 'escapeKeyDown')}
        />
        <SearchState value={searchState} query={query} />
      </div>
    </Drawer>
  );
});

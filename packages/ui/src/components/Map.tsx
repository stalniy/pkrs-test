import React from 'react';
import { GoogleMap, Marker, useLoadScript } from '@react-google-maps/api';
import BarLoader from 'react-spinners/BarLoader';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import geo, { SearchResult } from '../services/geo';

const useStyles = makeStyles(theme => createStyles({
  loader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%'
  }
}));

type EventListener = (event: google.maps.MouseEvent) => void;
interface MapProps {
  results?: SearchResult[]
  children?: any
  onLongClick?: EventListener
}

function calcBounds(results: SearchResult[]) {
  const northeast = results[0].location;
  const southwest = results[0].location;

  for (let i = 1; i < results.length; i++) {
    const latLng = results[i].location;

    if (latLng.lat > northeast.lat) northeast.lat = latLng.lat;
    if (latLng.lat < southwest.lat) southwest.lat = latLng.lat;
    if (latLng.lng > northeast.lng) northeast.lng = latLng.lng;
    if (latLng.lng < southwest.lng) southwest.lng = latLng.lng;
  }

  return new google.maps.LatLngBounds(southwest, northeast);
}

function addLongClickListener(map: google.maps.Map, listener: EventListener) {
  let timerId: NodeJS.Timeout;
  const clear = () => clearTimeout(timerId);
  const mouseDownSubscription = map.addListener('mousedown', (event) => {
    timerId = setTimeout(() => listener(event), 500);
  });
  const mouseUpSubscription = map.addListener('mouseup', clear);
  const dragSubscription = map.addListener('dragstart', clear);

  return () => {
    mouseDownSubscription.remove();
    mouseUpSubscription.remove();
    dragSubscription.remove();
  };
}

let LAST_BOUNDS = {
  east: 59.09666316949502,
  north: 70.73318866117386,
  south: -1.7447877089580588,
  west: -74.14552433050498
};

function useBounds(map: google.maps.Map | undefined, results: SearchResult[]) {
  React.useEffect(() => {
    if (!map) return;

    if (results.length) {
      map.fitBounds(calcBounds(results));
      map.setZoom(15);
      LAST_BOUNDS = map.getBounds()!.toJSON();
    } else {
      map.fitBounds(LAST_BOUNDS);
    }
  }, [map, results]);
}

function useFeatureDetails(map: google.maps.Map | undefined) {
  React.useEffect(() => {
    if (!map) return;
    addLongClickListener(map, (event) => {
      geo.reverseSearch(event.latLng.toJSON())
        .then(([result]) => {
          const content = result
            ? result.title
            : 'Unknown location';
          const info = new google.maps.InfoWindow({ content });
          const marker = new google.maps.Marker({
            map,
            position: result.location,
            title: result.title
          });
          info.addListener('closeclick', () => marker.setMap(null));
          info.open(map, marker);
        })
    });
  }, [map]);
}

export default (props: MapProps) => {
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_API_KEY
  });
  const [map, setMap] = React.useState<google.maps.Map>();
  const styles = useStyles();
  const results = props.results || [];

  useBounds(map, results);
  useFeatureDetails(map);

  if (loadError) {
    return <div>The app is not available right now. Please check your connection status.</div>
  }

  if (!isLoaded) {
    return (
      <div className={styles.loader}>
        <BarLoader color="#4285F4" />
      </div>
    );
  }

  return (
    <GoogleMap
      mapContainerStyle={{ width: '100%', height: '100%' }}
      onLoad={setMap}
      options={{
        fullscreenControl: false,
        mapTypeControl: false,
        streetViewControl: false,
        clickableIcons: false
      }}
    >
      {results.map(result => <Marker position={result.location} key={result.title} />)}
      {props.children}
    </GoogleMap>
  );
};

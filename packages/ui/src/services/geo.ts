import http from './http';

export interface SearchResult extends ReverseSearchResult {
  isPartial: boolean
}

export interface ReverseSearchResult {
  title: string
  types: string[]
  location: {
    lat: number
    lng: number
  }
}

export interface Point {
  lat: number,
  lng: number
}

interface Response<T extends ReverseSearchResult> {
  matches: T[]
}

export default {
  search(address: string) {
    return http.get<Response<SearchResult>>('/v1/location', { params: { address } })
      .then(response => response.data.matches);
  },

  reverseSearch(location: Point) {
    const params = { ...location };
    return http.get<Response<ReverseSearchResult>>('/v1/place', { params })
      .then(response => response.data.matches)
  }
};

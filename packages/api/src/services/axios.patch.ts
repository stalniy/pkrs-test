import 'axios';
import { Options } from 'opossum';

declare module 'axios' {
  export interface AxiosRequestConfig {
    cache?: true
    breaker?: Options
  }
}

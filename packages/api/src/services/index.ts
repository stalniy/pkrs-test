import { FastifyInstance } from 'fastify';
import http from './http';
import config from 'config';
import './fastify.patch';
import './axios.patch';

export { Http } from './http';

export default (fastify: FastifyInstance) => {
  fastify.decorate('http', http);
  fastify.decorate('config', config);
};

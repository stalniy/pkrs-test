import axios, { AxiosInstance, AxiosAdapter, AxiosRequestConfig, AxiosResponse } from 'axios';
import CircuitBreaker, { Options } from 'opossum';
import LruCache from 'lru-cache';

function requestKey(config: AxiosRequestConfig) {
  const suffix = config.params ? `.${JSON.stringify(config.params)}` : '';
  return config.url + suffix;
}

function cached(request: AxiosAdapter) {
  const store = new LruCache<string, AxiosResponse>({
    maxAge: 60 * 60 * 1000,
    max: 10000,
  });

  return ({ cache, ...config }: AxiosRequestConfig & { cache?: true }) => {
    if (!cache || config.method !== 'get') {
      return request(config);
    }

    const keyPrefix = config.headers['X-Language'] || '';
    const key = keyPrefix + requestKey(config);
    const cachedResponse = store.get(key);

    if (!cachedResponse) {
      return request(config)
        .then((response) => {
          store.set(key, response);
          return response;
        });
    }

    return Promise.resolve(cachedResponse);
  };
}

function withCircuitBreaker(request: AxiosAdapter) {
  type AxiosCircuitBreaker = CircuitBreaker<[AxiosRequestConfig], AxiosResponse>;
  const breakers = new LruCache<string, AxiosCircuitBreaker>({
    max: 100
  });

  return ({ breaker, ...config }: AxiosRequestConfig & { breaker?: Options }) => {
    if (!breaker) {
      return request(config);
    }

    const key = JSON.stringify(breaker);

    if (!breakers.has(key)) {
      breakers.set(key, new CircuitBreaker(request, breaker) as unknown as AxiosCircuitBreaker);
    }

    return breakers.get(key)!.fire(config);
  }
}

export type Http = AxiosInstance;

export default axios.create({
  adapter: cached(withCircuitBreaker(axios.defaults.adapter!))
});

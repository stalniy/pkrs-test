import { Point } from '../schema/Point';

export function parsePoint(point: string): Point {
  const [lat, lng] = point.split(',').map(Number);
  return { lat, lng };
}

export function parseBounds(bounds?: string): undefined | [Point, Point] {
  if (!bounds) {
    return undefined;
  }

  return bounds.split('|').map(parsePoint) as [Point, Point];
}

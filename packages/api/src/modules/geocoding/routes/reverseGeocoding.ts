import { FastifyPluginAsync } from 'fastify';
import { InferFromSchema } from 'json-schema-to-ts';
import onError from '../hooks/onError';
import { Point, Lang } from '../schema';

export const schema = {
  querystring: {
    type: 'object',
    required: Point.required,
    properties: {
      ...Point.properties,
      language: Lang
    }
  },
  response: {
    200: {
      type: 'object',
      properties: {
        matches: {
          type: 'array',
          items: {
            type: 'object',
            required: ['title', 'types', 'location'],
            properties: {
              title: { type: 'string' },
              types: { type: 'array', items: { type: 'string' } },
              location: Point
            }
          }
        }
      }
    }
  }
} as const;

type Query = InferFromSchema<typeof schema['querystring']>;

export default (async (fastify) => {
  fastify.get<{ Querystring: Query }>('/place', {
    schema,
    onError,
    async handler(req) {
      const { lat, lng, language } = req.query;
      const matches = await this.geocoder.geodecode({ lat, lng }, {
        language,
      });
      return { matches };
    }
  });
}) as FastifyPluginAsync;

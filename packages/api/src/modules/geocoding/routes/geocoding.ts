import { FastifyPluginAsync } from 'fastify';
import { InferFromSchema } from 'json-schema-to-ts';
import { parseBounds } from '../utils/parseGeo';
import onError from '../hooks/onError';
import { Point, Lang } from '../schema';

export const schema = {
  querystring: {
    type: 'object',
    required: ['address'],
    properties: {
      address: { type: 'string' },
      bounds: { type: 'array', items: Point, minItems: 2, maxItems: 2 },
      language: Lang
    }
  },
  response: {
    200: {
      type: 'object',
      properties: {
        matches: {
          type: 'array',
          items: {
            type: 'object',
            required: ['title', 'types', 'location', 'isPartial'],
            properties: {
              title: { type: 'string' },
              types: { type: 'array', items: { type: 'string' } },
              isPartial: { type: 'boolean' },
              location: Point
            }
          }
        }
      }
    }
  }
} as const;

type Query = InferFromSchema<typeof schema['querystring']>;

export default (async (fastify) => {
  fastify.get<{ Querystring: Query }>('/location', {
    schema,
    onError,
    async preValidation(req) {
      if (req.query.bounds) {
        const rawQuery = req.query as unknown as { bounds: string };
        req.query.bounds = parseBounds(rawQuery.bounds);
      }
    },
    async handler(req) {
      const { language, bounds } = req.query;
      const matches = await this.geocoder.geocode(req.query.address, {
        language,
        bounds: bounds as [Point, Point] | undefined,
      });
      return { matches };
    }
  });
}) as FastifyPluginAsync;

import { FastifyPluginAsync } from 'fastify';
import { InferFromSchema } from 'json-schema-to-ts';
import { parsePoint } from '../utils/parseGeo';
import { Point } from '../schema';
import calcDistance from '../services/calcDistance';

export const schema = {
  querystring: {
    type: 'object',
    required: ['a', 'b'],
    properties: {
      a: Point,
      b: Point,
      unit: { type: 'string', enum: ['ml', 'km'] }
    }
  },
  response: {
    200: {
      type: 'object',
      properties: {
        distance: { type: 'number' }
      }
    }
  }
} as const;

type Query = InferFromSchema<typeof schema['querystring']>;

export default (async (fastify) => {
  fastify.get<{ Querystring: Query }>('/distance', {
    schema,
    async preValidation(req) {
      const rawQuery = req.query as unknown as { a: string, b: string };
      req.query.a = parsePoint(rawQuery.a);
      req.query.b = parsePoint(rawQuery.b);
    },
    async handler(req) {
      const distance = calcDistance(req.query.a, req.query.b, {
        unit: req.query.unit || 'km'
      });

      return { distance };
    }
  });
}) as FastifyPluginAsync;

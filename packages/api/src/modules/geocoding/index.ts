import { FastifyPluginAsync } from 'fastify';
import geocoding from './routes/geocoding';
import reverseGeocoding from './routes/reverseGeocoding';
import distance from './routes/distance';
import { GoogleGeoCoder } from './services/GoogleGeocoder';
import './services/fastify.patch';

export default (async (fastify) => {
  fastify
    .decorate('geocoder', new GoogleGeoCoder(fastify.http, fastify.config.get('geocoding')))
    .register(geocoding, { prefix: '/v1' })
    .register(reverseGeocoding, { prefix: '/v1' })
    .register(distance, { prefix: '/v1' })
  ;
}) as FastifyPluginAsync;

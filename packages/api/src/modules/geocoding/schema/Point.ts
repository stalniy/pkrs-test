import { InferFromSchema } from 'json-schema-to-ts';

export const Point = {
  type: 'object',
  required: ['lat', 'lng'],
  properties: {
    lat: { type: 'number', minimum: -90, maximum: 90 },
    lng: { type: 'number', minimum: -90, maximum: 90 },
  }
} as const;

export type Point = InferFromSchema<typeof Point>;

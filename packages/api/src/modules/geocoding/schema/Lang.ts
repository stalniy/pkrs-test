import { JsonSchemaTypeOf } from 'json-schema-to-ts';

export const Lang = { type: 'string', enum: ['uk', 'en'] } as const;
export type Lang = JsonSchemaTypeOf<typeof Lang>;

import { Http } from '../../../services';
import { GeocoderExhaustedError, GeocoderUnexpectedError } from './errors';
import { Point } from '../schema/Point';

const URL = 'https://maps.googleapis.com/maps/api/geocode/json';

interface Config {
  apiKey: string
  circuitBreaker?: Record<string, unknown>
}

interface GeocodingOptions {
  bounds?: [Point, Point],
  language?: string
}

interface GeodecodeOptions {
  language?: string
}

interface GeocodingResults {
  status: 'OK'
    | 'ZERO_RESULTS'
    | 'UNKNOWN_ERROR'
    | 'REQUEST_DENIED'
    | 'OVER_QUERY_LIMIT'
    | 'OVER_DAILY_LIMIT'
    | 'INVALID_REQUEST'
  error_message?: string
  results: Array<{
    formatted_address: string
    types: string[]
    partial_match?: boolean
    geometry: {
      location: Point,
    }
  }>
}

function handleHttpErrors(error: Error & { response?: { status: number }}) {
  if (!error.response) {
    throw error;
  }

  const { status } = error.response;

  if (status >= 500) {
    throw new GeocoderUnexpectedError('Unexpected error', error);
  }

  if (status >= 400) {
    throw new GeocoderUnexpectedError('Looks like Google API was changed and we need to adapt GoogleGeocoder service.', error);
  }
}

type GeocodingMapper = (result: GeocodingResults['results'][number]) => object;
function handleGeocodingResponse(response: GeocodingResults, mapper: GeocodingMapper) {
  switch (response.status) {
    case 'ZERO_RESULTS':
      return [];
    case 'OVER_DAILY_LIMIT':
    case 'OVER_QUERY_LIMIT':
    case 'REQUEST_DENIED':
      throw new GeocoderExhaustedError(response.error_message || response.status);
    case 'UNKNOWN_ERROR':
    case 'INVALID_REQUEST':
      throw new GeocoderUnexpectedError(response.error_message!);
    case 'OK':
      return response.results.map(mapper);
    default:
      throw new GeocoderUnexpectedError(`Unexpected Google API status "${response.status}"`);
    }
}

export class GoogleGeoCoder {
  constructor(
    private _http: Http,
    private _config: Config
  ) {}

  geocode(address: string, options: GeocodingOptions = {}) {
    const params: Record<string, unknown> = {
      address,
      key: this._config.apiKey,
      language: options.language || 'en',
    };

    if (options.bounds) {
      const [south, north] = options.bounds;
      params.bounds = `${south.lat},${south.lng}|${north.lat},${north.lng}`;
    }

    return this._http.get<GeocodingResults>(URL, { params, cache: true, breaker: this._config.circuitBreaker })
      .then((response) => {
        return handleGeocodingResponse(response.data, (result) => ({
          title: result.formatted_address,
          types: result.types,
          location: result.geometry.location,
          isPartial: !!result.partial_match
        }));
      })
      .catch(handleHttpErrors);
  }

  geodecode(location: Point, options: GeodecodeOptions) {
    const params = {
      language: options.language,
      latlng: `${location.lat},${location.lng}`,
      key: this._config.apiKey,
    };

    return this._http.get(URL, { params, cache: true, breaker: this._config.circuitBreaker })
      .then((response) => {
        return handleGeocodingResponse(response.data, (result) => ({
          title: result.formatted_address,
          types: result.types,
          location: result.geometry.location,
       }));
      })
      .catch(handleHttpErrors)
  }
}

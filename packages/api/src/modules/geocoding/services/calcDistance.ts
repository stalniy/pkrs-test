import { Point } from '../schema';

const toRadians = (value: number) => value * Math.PI / 180;
const pointToRadians = (point: Point) => ({ lat: toRadians(point.lat), lng: toRadians(point.lng) });

const EARTH_RADIUS_IN_MILES = 3958.8;
const EARTH_RADIUS_IN = {
  km: EARTH_RADIUS_IN_MILES * 1.60934,
  ml: EARTH_RADIUS_IN_MILES
};

interface Options {
  unit: keyof typeof EARTH_RADIUS_IN
}

export default function calcDistance(pointA: Point, pointB: Point, options: Options) {
  const a = pointToRadians(pointA);
  const b = pointToRadians(pointB);
  const lngDiff = b.lng - a.lng;
  const k1 = Math.pow(Math.cos(b.lat) * Math.sin(lngDiff), 2) + Math.pow(
    Math.cos(a.lat) * Math.sin(b.lat) -
    Math.sin(a.lat) * Math.cos(b.lat) * Math.cos(lngDiff),
    2
  );
  const k2 = Math.sin(a.lat) * Math.sin(b.lat) +
    Math.cos(a.lat) * Math.cos(b.lat) * Math.cos(lngDiff);

  return EARTH_RADIUS_IN[options.unit] * Math.atan2(Math.sqrt(k1), k2);
}

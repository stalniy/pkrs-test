export class GeocoderUnexpectedError extends Error {
  public readonly originalError?: Error;

  constructor(message: string, originalError?: Error) {
    super(message);
    this.originalError = originalError;
  }
}
export class GeocoderExhaustedError extends Error {}

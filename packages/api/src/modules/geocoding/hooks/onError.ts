import { onErrorHookHandler } from 'fastify';
import { GeocoderExhaustedError } from '../services/errors';

export default (async (_, res, error) => {
  if (error instanceof GeocoderExhaustedError) {
    res.status(503);
  }
}) as onErrorHookHandler

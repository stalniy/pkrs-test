import config from 'config';
import { createServer } from './index';

createServer(config.get('server'))
  .then(async (server) => {
    try {
      await server.listen(config.get('server.port'));
    } catch (error) {
      server.log.error(`Unable to start application server at ${config.get('server.port')}`)
      server.log.error(error);
    }
  });

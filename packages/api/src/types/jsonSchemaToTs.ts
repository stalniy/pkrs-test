type JsonSchemaObject = {
  type: 'object'
  required?: Readonly<string[]>
  properties: {
    [key: string]: JsonSchema
  }
};
type JsonSchemaArray = {
  type: 'array'
  items: JsonSchema
};
type JsonSchemaScalar = { type: string };
type JsonSchemaEnum = { enum: Readonly<string[]> };
type JsonSchema = JsonSchemaScalar | JsonSchemaObject | JsonSchemaArray;

type JsonSchemaTypeMapping = {
  string: string
  number: number
  boolean: boolean
};

export type JsonSchemaTypeOf<T extends JsonSchema> = T extends JsonSchemaObject
  ? InferFromSchema<T>
  : T extends JsonSchemaArray
    ? Array<JsonSchemaTypeOf<T['items']>>
    : T['type'] extends keyof JsonSchemaTypeMapping
      ? T extends JsonSchemaEnum
          ? T['enum'][number]
          : JsonSchemaTypeMapping[T['type']]
      : never;
type RequiredProps<T extends Readonly<string[]> | undefined> = Exclude<T, undefined>[number];

export type InferFromSchema<T extends JsonSchemaObject> = {
  [K in RequiredProps<T['required']>]: JsonSchemaTypeOf<T['properties'][K]>
} & {
  [O in Exclude<keyof T['properties'], RequiredProps<T['required']>>]?: JsonSchemaTypeOf<T['properties'][O]>
};

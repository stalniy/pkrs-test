import fastify, { FastifyServerOptions } from 'fastify';
import cors from 'fastify-cors';
import { geocoding } from './modules';
import injectServices from './services';

export async function createServer(options: FastifyServerOptions) {
  const server = fastify(options);

  injectServices(server);
  server.register(cors, {
    origin: true,
    maxAge: 600,
  });
  server.register(geocoding, { prefix: '/api' });

  return server;
}
